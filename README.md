# devops-101

Intro into DevOps with kubernetes 

- overview of the tools
- how to setup local env
- building first kubernetes cluster


## Tools

- minikube
- kubectl
- pip install agentk

## Links

- https://kubernetes.io/docs/concepts/workloads/pods/pod/
- https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/
